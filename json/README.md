Die Schemata werden gemäß dem derzeitigen Stand von "OGC Features and Geometries JSON" (JSON-FG) erzeugt. Dieser ["candidate standard"](https://github.com/opengeospatial/ogc-feat-geo-json) befindet sich in einer frühen Entwurfsphase.

Das Schema "features" beschreibt alle XTrasse-Features im Detail. Das Schema "featurecollection" ist eine Auflistung aller XTrasse-Features und kann zur Validierung des JSON-Dokuments verwendet werden (s. [hier](https://gitlab.opencode.de/xleitstelle/xtrasse/anwendungen/-/tree/main/testdaten)).






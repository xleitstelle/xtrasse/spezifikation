Die XML-Schemata sind ebenso im [Repository](https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/) der GDI-DE verfügbar.

Die Instanzen werden mit dem Eintrag  xsi:schemaLocation="http://www.xtrasse.de/2.0 https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/XTrasse.xsd" gegen die Schemata validiert.
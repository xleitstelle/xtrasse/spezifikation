# Standard XTrasse

XTrasse ist ein **Datenstandard** für die Modellierung von Leitungsnetzen. Als **Datenaustauschformat** unterstützt XTrasse den verlustfreien Transfer von Leitungsplänen zwischen unterschiedlichen IT-Systemen sowie deren internetgestützte Bereitstellung. Der Standard ermöglicht somit den Austausch von vollvektoriellen georeferenzierten Planwerken zwischen Leitungsunternehmen und der Verwaltung im Rahmen von Genehmigungsverfahren sowie die Bereitstellung von Informationen über den Ausbau von Leitungsnetzen. 

XTrasse ist keine Software, sondern bildet die Grundlage für die Entwicklung von Anwendungen zur Erzeugung, Bereitstellung und Nutzung XTrasseGML-konformer Daten. Die XLeitstelle unterstützt die Softwareentwicklung mit der Bereitstellung von [XTrasse Anwendungen](https://gitlab.opencode.de/xleitstelle/xtrasse/anwendungen) für eine Produktionsumgebung.

XTrasseGML wird in der Datenbeschreibungssprache UML mit der Zielsetzung modelliert, die in den relevanten gesetzlichen Grundlagen vorgegebenen Inhalte der Leitungs-/Trassenpläne zu formalisieren und auf vorgegebene Objektklassen abzubilden. Aus dem XTrasse Anwendungsschema in UML werden XML-Schema-Dateien erzeugt, gegen die XTrasseGML-konforme Pläne valide (gültig) sein müssen.

## Anwendungsfälle der Modellierung

Die Modellierung des Standards XTrasse 2.0 basiert auf insgesamt sechs Anwendungsfällen. Zwei betreffen den Ausbau der Telekommunikationsinfrastruktur:

- **Breitbandausbau**: Trassenplanungen für den Ausbau von Glasfasernetzen lassen sich von der überörtlichen Konzeption bis zur Detailtiefe der Zustimmungsverfahren nach TKG abbilden in Bezug auf deren räumlichen Lage, die Spezifizierung der Trasse (inkl. ihrer punktförmigen Infrastrukturkomponenten) sowie prüfungsrelevante Informationen zur Bauausführung. Darüber hinaus können Bestandsleitungen aller Leitungssparten erfasst werden.

- **Infrastrukturatlas**: Datenlieferungen von Leitungsnetzbetreibern für den von der Bundesnetzagentur betreuten [Infrastrukturatlas](https://isa.bundesnetzagentur.de/home/#/) können im Datenformat XTrasseGML erfolgen.

Planungen zum großräumigen Ausbau von Leitungsnetzen bilden drei Anwendungsfälle:

- **Raumverträglichkeitsprüfung**: Der Ausbau linienförmiger Infrastrukturen beginnt mit einem mehrere Stufen umfassenden Planungsprozess von Trassenkorridoren. Dieser wird abgebildet im Hinblick auf die einzelnen Korridorsegmente und daraus entwickelte Varianten, den zuletzt festgelegten Antragskorridor sowie Kategorien der im Korridor angetroffenen Raumwiderstände und Konflikte.

- **Infrastrukturgebieteplan**: Alternativ zur Raumverträglichkeitsprüfung können für Energieleitungen Infrastrukturgebiete festgelegt werden. (Dieser Anwendungsfall befindet sich noch in der gesetzlichen Abstimmung.)

- **Planfeststellung**: Beginnend an der Schnittstelle zur Raumverträglichkeitsprüfung bzw. dem Infrastrukturgebieteplan kann der Ausbau trassenförmiger Energieinfrastrukturen in verschiedenen Maßstabsebenen geplant werden. Erfasst werden der räumliche Verlauf geplanter Trassen, Attribute der Leitungen und Infrastrukturkomponenten, die Bauausführung sowie Anschlusspunkte und Leitungen im Bestand. Der abschließende Antrag auf Planfeststellung (und Baugenehmigung) wird nicht mehr von XTrasse abgebildet.

Die Erfassung von Bestandsinfrastruktur in einem eigenständigen Plan ist der sechste Anwendungsfall:

- **Bestandsplan**: Leitungspläne der verschiedenen Sparten lassen sich in einem Bestandsmodell zusammenführen. Das TrasseGML kann anschließend in ein IFC-Modell transformiert werden.


## Auslieferungsgegenstände

Die Spezifikation des Standards XTrasse umfasst folgende Bestandteile:

- **UML-Modell (EnterpriseArchitect® Format)**: Das UML-Modell enthält die Datenstrukturen mit den entsprechenden semantischen Definitionen, die benötigt werden, um Planwerke gemäß dem Zweck des Standards zu repräsentieren. Das UML-Modell des Standards XTrasse wird u.a. für die Produktion des Objektartenkataloges und der Schema-Dateien eingesetzt.

- **Objektartenkatalog (html)**: Der Objektartenkatalog enthält die Beschreibung aller abstrakten Oberklassen, von denen alle Klassen der Fachschemata abgeleitet sind, sowie allgemeine Objektarten, Datentypen und Enumerationen, die in verschiedenen Fachschemata verwendet werden. Die Darstellung der Klassen und Attribute erfolgt ebenso über UML-Diagramme. 

- **XML-Schemadateien (xsd):** Die XML-Schemadateien definieren das XML-Format XTrasse-bezogener Daten (XTrasseGML). Die XML-Schemata beschreiben die Objekttypen, deren Daten präsentiert und die von der Anwendung verarbeitet werden sollen. Die XML-Schemata bilden die Grundlage für die Implementierung von XTrasse-Schnittstellen in Fachanwendungen und GIS-Systemen.

- **JSON-Schemadateien (json):** Die JSON-Schemadateien definieren das JSON-FG-Format XTrasse-bezogener Daten (XTrasseJSON). Die zwei JSON-Schemata beschreiben die Objekte und ihre Auflistung in einer FeatureCollection. Die JSON-Schemata werden zukünftig insbesondere der Implementierung von XTrasse-Schnittstellen in Onlineportalen dienen.



## Releases

Die zuvor genannten Auslieferungsgegenstände des Standards XTrasse 2.0 befinden sich noch im Entwurfsstadium. Die Veröffentlichung als Release ist für das dritte Quartal 2024 geplant.

Das Release des Standards XTrasse 1.0 ist [hier](https://gitlab.opencode.de/xleitstelle/xtrasse/spezifikation/-/releases) abrufbar.

## Roadmap

Mit dem Versionssprung zu XTrasse 2.0 wurde das Modell modularer aufgebaut, um den Gestaltungsspielraum für die fachliche Weiterentwicklung der einzelnen Anwendungsfälle zu erhöhen. Ebenso lassen sich neue Anwendungsfälle leichter in das Modell integrieren. 

## Zusammenhang mit "XStandards" und OZG-Umsetzung

Der IT-Planungsrat hatte 2019 die Erweiterung der IT-Standards XPlanung und XBau im Rahmen der Umsetzung des OZG Digitalisierungslabors Breitbandausbau veranlasst und 2020 die verbindliche Anwendung der neuen Standards XTrasse und XBreitband für den Anwendungsfall "Zustimmung nach Telekommunikationsgesetz" im Kontext der OZG Verwaltungsleistung Breitbandausbau beschlossen. XTrasse basiert auf XPlanung, XBreitband auf XBau.

[XBreitband](https://www.xrepository.de/details/urn:xoev-de:it-plr:standard:xbau-tiefbau) unterstützt den Nachrichten- und Datenaustausch in Zustimmungs- und Genehmigungsverfahren, z.B. nach Telekommunikationsgesetz oder den Straßen- und Wegegesetzen der Länder. Die Kommunikation zwischen Fachanwendungen von Antragsstellern und Behörden wird in Form strukturierter Nachrichten abgebildet. Ein Trassenplan in XTrasseGML ist zentraler Bestandteil einer XBreitband-Nachricht "Antrag auf Zustimmung nach TKG § 127", der u.a. im Anhang der Nachricht übermittelt werden kann. 

XTrasse soll zukünftig im [OZG-Breitbandportal](https://www.breitband-portal.de) implementiert werden. Ein Trassenplan in XTrasseGML/XTrasseJSON lässt sich dann auch - alternativ zu nicht standardisierten Dateiformaten - in einer GIS-Komponente hochladen. 






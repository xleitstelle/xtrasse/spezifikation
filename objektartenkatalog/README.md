
Der Objektartenkatalog dokumentiert und erläutert die XML-Schemata des Standards XTrasse in einem HTML-Dokument. Der Katalog enthält weiterhin die UML-Diagramme aus dem Datenmodell.

Geöffnet wird das Dokument im Browser über die Datei "index.html". 
Die linke Spalte listet die "Pakete" und "Unterpakete" in der Struktur des Datenmodells. Die benachbarte Spalte enthält die Objekte des gesamten Modells oder die des ausgewählten Pakets. 

Das Hauptfenster zeigt an: 
- auf der Ebene des Gesamtmodells die Pakete,
- auf der Ebene der Pakete die untergeordneten Pakete und ein UML-Diagramm,
- bei untergeordneten Paketen die Objektarten und UML-Diagramme,
- bei den Objektarten die Attribute eines Objektes zuerst in tabellarischer und unterhalb in detaillierter Form. Die über Attribute eingebundenen Enumerationen werden hier ebenfalls als Tabelle aufgeführt.






